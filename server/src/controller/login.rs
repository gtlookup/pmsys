use actix_web::{get, post, HttpResponse};
use actix_web::web::Json;
use maplit::*;
use crate::service::login::Vo;
use crate::utils::valid::Valid;
use crate::result::R;


#[post("/login")]
pub async fn login(vo: Json<Vo>) -> HttpResponse {
    if !vo.is_valid() {
        return R::new(false, "", vo.to_map());
    }

    let d = crate::service::login::login(&vo).await;

    R::new(d.0, d.1.as_str(), hashmap! { "token" => d.2 })
}
#[get("/is/login")]
pub async fn is_login() -> HttpResponse {
    R::new(true, "", "")
}