#[macro_use]
extern crate rbatis;
#[macro_use]
extern crate lazy_static;

use rbatis::rbatis::Rbatis;
use crate::config::config::AppToml;
lazy_static! {
    pub static ref DB: Rbatis = Rbatis::new();
    pub static ref CONFIG: AppToml = AppToml::new().read();
}
pub mod controller;
pub mod entity;
pub mod service;
pub mod utils;
pub mod result;
pub mod config;