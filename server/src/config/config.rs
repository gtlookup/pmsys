use std::fs::File;
use std::io::Read;

#[derive(Debug, serde::Deserialize)]
pub struct AppToml {
    pub db_url: String,
    pub server_bind: String,
    pub secure: String,
    pub token_exp: i64,
    pub page_size: u64
}

impl AppToml {
    pub fn new() -> Self {
        Self {
            db_url: String::new(),
            secure: String::new(),
            server_bind: String::new(),
            token_exp: 0,
            page_size: 20
        }
    }
    /// 读取配置文件
    pub fn read(&mut self) -> Self {
        let mut s = String::new();
        File::open("app_config.toml").unwrap().read_to_string(&mut s).unwrap(); // 读取文件
        let cnf: Self = toml::from_str(&s).unwrap();
        cnf
    }
}