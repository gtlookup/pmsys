use validator::Validate;
use serde::Deserialize;
use crate::utils::password::password_verify;
use crate::service::user::UserService;

#[derive(Debug, Validate, Deserialize)]
pub struct Vo {
    #[validate(length(max = 20, min = 1))]
    pub account: String,
    #[validate(length(max = 20, min = 1))]
    pub password: String
}

/// 登陆操作
pub async fn login(vo: &Vo) -> (bool, String, String) {
    // 取用户
    if let Some(ob) = UserService::by_account(vo.account.clone()).await {
        if !password_verify(
            vo.password.clone(),
            ob.password.clone().unwrap_or(String::new())) {
            return (false, "密码不正确！".to_string(), String::new());
        }

        let token = crate::utils::token::create_token(ob);

        return (true, String::new(), token);
    }

    (false, "用户不存在！".to_string(), String::new())
}