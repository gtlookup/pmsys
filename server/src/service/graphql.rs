use async_graphql::{Schema, EmptyMutation, EmptySubscription, MergedObject};
use actix_web::{web, post, HttpRequest};
use crate::entity::sys_user::SysUser;
use actix_web::http::HeaderValue;

#[derive(MergedObject, Default)]
pub struct PMsysQuery(SysUser);

pub type QuerySchema = Schema<PMsysQuery, EmptyMutation, EmptySubscription>;

#[post("/graphql")]
pub async fn index(
    schema: web::Data<QuerySchema>,
    req: HttpRequest,
    g_req: async_graphql_actix_web::Request
) -> async_graphql_actix_web::Response {
    let token = req.headers()
        .get("Authorization")
        .unwrap_or(&HeaderValue::from_str("").unwrap())
        .to_str().unwrap_or("").to_string();
    schema.execute(g_req.into_inner().data(token)).await.into()
}