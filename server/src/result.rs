use serde::Serialize;
use actix_web::HttpResponse;

#[derive(Serialize, Debug)]
pub struct R<T: Serialize> {
    pub message: String,
    pub is_success: bool,
    pub data: T
}

impl<T: Serialize> R<T> {
    pub fn new(b: bool, s: &str, o: T) -> HttpResponse where T: Serialize {
        HttpResponse::Ok().json(Self { is_success: b, message: s.to_string(), data: o })
    }
}
