use async_graphql::{Schema, EmptyMutation, EmptySubscription};
use actix_cors::Cors;
use actix_web::{HttpServer, App};
use server::service::graphql::{PMsysQuery, index};
use server::controller::login::{login, is_login};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let cnf = &server::CONFIG;
    server::DB.link(cnf.db_url.as_str()).await?;
    let schema = Schema::new(
        PMsysQuery::default(),
        EmptyMutation,
        EmptySubscription
    );
    let init_app = move || {
        App::new()
            .wrap(server::utils::token::AuthFilter)
            .data(schema.clone())
            .wrap(Cors::permissive())
            .service(index)
            .service(login)
            .service(is_login)
    };
    HttpServer::new(init_app).bind(cnf.server_bind.as_str())?.run().await
}