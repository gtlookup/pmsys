
/// 获取盐
pub fn get_salt(size: u8) -> String {
    use rand::{thread_rng, Rng};
    use rand::distributions::Alphanumeric;

    let mut rng = thread_rng();
    std::iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .map(char::from)
        .take(size as usize)
        .collect()
}

/// 密码加密
pub fn password_hash(pwd: &str) -> String {
    bcrypt::hash(pwd, 4).unwrap_or("".to_string())
}

/// 密码验证
pub fn password_verify(pwd: String, hash: String) -> bool {
    bcrypt::verify(pwd, hash.as_str()).unwrap_or(false)
}