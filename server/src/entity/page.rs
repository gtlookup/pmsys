use async_graphql::*;
use crate::entity::sys_user::SysUser;

#[derive(SimpleObject, Debug)]
#[graphql(concrete(name="SysUser", params(SysUser)))]
pub struct Page<T: OutputType> {
    pub record: Vec<T>,
    pub total: u64,
    pub pages: u64,
    pub page_no: u64,
    pub page_size: u64
}

impl<T: OutputType> From<rbatis::plugin::page::Page<T>> for Page<T> {
    fn from(p: rbatis::plugin::page::Page<T>) -> Self {
        Self {
            record: p.records,
            total: p.total,
            pages: p.pages,
            page_no: p.page_no,
            page_size: p.page_size
        }
    }
}