# 目地

1）熟悉常年霸榜最快web框架，`actix-web`

2）熟悉 `rbatis`。虽然不及 `diesel` 和 `sqlx` 热度高，但之前一直有 `mybatis plus` 情节在，所以选择了 `rbatis`

3）熟悉 `graphsql`，这是防前后端撕逼利器，用的大神 @老油条 写的 `async-graphql`；也采用少量的 `RESTFul`

4）熟悉基于 `WebAssembly` 的前端框架 `yew`，用起来像 `react`。因为要学 `rust`，所以前端也采用拿 `rust` 写代码的 `yew` 框架

5）熟悉一下 `jsonwebtoken`。以前需要把 token 存到 session 里，然后设置过期时间；这回不需要存了，因为 token 里本身就带过期时间，后端在解析时就能知道时否过期

# 描述

用 Rust 语言开发一个功能简单的 `项目管理` demo。

# 角色

- Boss：用来查看所有项目进度情况
- Manager：管理自己所担当的项目，及进度情况
  - 创建项目，项目内人员管理
  - 创建人，并赋予角色（Leader 或 Worker）
  - 查看 Leader / Worker 的任务及进度
- Leader：创建任务，任务分配，查看自己所担当任务，及进度情况
- Worker：查看自己所担当任务，及进度情况，提交任务进度

# 表结构

```mysql
# 人员表
CREATE TABLE `sys_user`  (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
   `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
   `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
   `version` int(11) NOT NULL,
   `is_delete` bit(1) NOT NULL DEFAULT b'0',
   `position` int(255) NOT NULL COMMENT '0: admin\r\n1: boss\r\n2: manager\r\n3: leader\r\n4: worker',
   `create_by` int(11) NOT NULL,
   `create_time` datetime(0) NOT NULL,
   `update_by` int(11) NOT NULL,
   `update_time` datetime(0) NOT NULL,
   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```

