function layout_init() {
	setTimeout(function() {
		let menu = $('.app-menu');

		// Toggle Sidebar
		$('[data-toggle="sidebar"]').click(function(event) {
			event.preventDefault();
			$('.app').toggleClass('sidenav-toggled');
		});

		// Activate sidebar tree view toggle
		$("[data-toggle='treeview']").click(function(event) {
			event.preventDefault();
			if(!$(this).parent().hasClass('is-expanded')) {
				menu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
			}
			$(this).parent().toggleClass('is-expanded');
		});

		// Set initial active toggle
		$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

		//Activate bootstrip tooltips
		$("[data-toggle='tooltip']").tooltip();
	}, 1000);
}

function error(s) {
	$.notify({title: "失败：", message: s, icon: 'fa fa-close'}, {type: "danger"});
}

function success(s) {
	$.notify({title: "成功：", message: s, icon: 'fa fa-close'}, {type: "success"});
	// swal(t, c, "success");
}

function swl_message(b, t, s) {
	let tt = b ? 'success' : 'error';
	swal(t, s, tt);
}

function confirm(s) {
	swal({
		title: "是否继续此操作？",
		// text: "You will not be able to recover this imaginary file!",
		type: "warning",
		showCancelButton: true,
		confirmButtonText: "是",
		cancelButtonText: "否",
		closeOnConfirm: false,
		closeOnCancel: true
	}, function(y) {
		if (y) {
			swal({
				title: '操作成功！',
				type: 'success'
			}, function () { eval(s) })
		}
	});
}

function close_modal(s) {$('#' + s).modal('hide')}

function select_clean(s) { $('#' + s).val('') }