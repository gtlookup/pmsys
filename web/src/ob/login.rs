#[derive(serde::Deserialize, Debug)]
pub struct LoginResult {
    pub account: Option<String>,
    pub password: Option<String>,
    pub token: Option<String>
}