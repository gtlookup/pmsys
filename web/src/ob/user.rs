use serde::{Deserialize, Serialize};
use crate::ob::ResultPage;

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct SysUser {
    pub id: Option<u32>,
    pub account: Option<String>,
    pub password: Option<String>,
    pub name: Option<String>,
    pub version: Option<u32>,
    pub position: Option<u32>,
    pub createTime: Option<u32>
}
impl SysUser {
    pub fn new() -> Self {
        Self {
            id: None,
            account: None,
            password: None,
            name: None,
            version: None,
            position: None,
            createTime: None
        }
    }
}

#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct UserListPage {
    pub findUserListPage: ResultPage<SysUser>
}