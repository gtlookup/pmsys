use serde::Deserialize;

pub mod user;
pub mod login;

/// 分页结果
#[derive(Debug, Deserialize, Clone, PartialEq)]
pub struct ResultPage<T> {
    pub record: Vec<T>,
    pub total: u64,
    pub pages: u64,
    pub pageNo: u64,
    pub pageSize: u64
}

/// 调 async-graphql 返回的正确信息
#[derive(Debug, Deserialize)]
pub struct ResultGraphql<T> { pub data: T }

/// 调 RESTFul API 返回的结果
#[derive(Deserialize, Debug)]
pub struct ResultApi<T> {
    pub message: String,
    pub is_success: bool,
    pub data: T
}