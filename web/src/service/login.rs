use crate::service::request::Http;
use yew::services::fetch::FetchTask;
use yew::Callback;
use serde_json::{json, Value};
use crate::error::Error;
use crate::ob::ResultApi;
use crate::ob::login::LoginResult;

type _LoginResult = Callback<Result<ResultApi<LoginResult>, Error>>;

pub struct LoginService(Http);

impl LoginService {
    pub fn new() -> Self { Self(Http::new()) }
    pub fn login(&mut self, acc: &String, pwd: &String, done: _LoginResult) -> FetchTask {
        let data = json!({"account": acc, "password": pwd});
        self.0.post::<ResultApi<LoginResult>, Value>("/login", data, done)
    }
}