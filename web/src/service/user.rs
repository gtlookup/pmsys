use yew::services::fetch::FetchTask;
use yew::Callback;
use serde_json::{json, Value};
use serde_json::value::Value::Null;
use crate::ob::ResultGraphql;
use crate::ob::user::{UserListPage, SysUser};
use crate::service::request::Http;
use crate::error::Error;

type _UserResult = Callback<Result<ResultGraphql<UserListPage>, Error>>;

pub const FIND_PAGE: &str = "findUserListPage";

pub struct UserService(Http);

impl UserService {
    pub fn new() -> Self { Self(Http::new()) }
    /// 取人员分页
    pub fn list_page(&mut self, p: u64, done: _UserResult) -> FetchTask {
        let q = r#"
            query x($page: Int!) {
                findUserListPage(page: $page) {
                    record { id, account, name, version, position, createTime },
                    total, pages, pageNo, pageSize, searchCount
                }
            }
        "#;

        let param = json!({"page": p});
        let data = json!({"query": q, "variables": param});

        self.0.post::<ResultGraphql<UserListPage>, Value>("/graphql", data, done)
    }
    /// 删除
    pub fn delete(&mut self, id: u32, done: _UserResult) -> FetchTask {
        let s = r#"
            query x($id: Int!, $page: Int!) {
                deleteUserById(id: $id),
                findUserListPage(page: $page) {
                    record { id, account, name, version, position, createTime },
                    total, pages, pageNo, pageSize, searchCount
                }
            }
        "#;

        let param = json!({"id": id, "page": 1});
        let data = json!({"query": s, "variables": param});

        self.0.post::<ResultGraphql<UserListPage>, Value>("/graphql", data, done)
    }

    pub fn save(&mut self, ob: SysUser, done: _UserResult) -> FetchTask {
        let s = r#"
            query x($ob: InputSaveUser!, $p: Int!) {
                save(ob: $ob),
                findUserListPage(page: $p) {
                    record { id, account, name, version, position, createTime },
                    total, pages, pageNo, pageSize, searchCount
                }
            }
        "#;

        let u = json!({
            "account": ob.account, "name": ob.name,
            "password": ob.password, "position": ob.position
        });

        let param = json!({"ob": u, "p": 1});
        let data = json!({"query": s, "variables": param});
        self.0.post::<ResultGraphql<UserListPage>, Value>("/graphql", data, done)
    }

    pub fn update(&mut self, ob: SysUser, done: _UserResult) -> FetchTask {
        let s = r#"
            query x($ob: InputUpdateUser!, $p: Int!) {
                update(ob: $ob),
                findUserListPage(page: $p) {
                    record { id, account, name, version, position, createTime },
                    total, pages, pageNo, pageSize, searchCount
                }
            }
        "#;

        let u = json!({
            "id": ob.id, "account": ob.account, "version": ob.version,
            "name": ob.name, "position": ob.position
        });

        let param = json!({"ob": u, "p": 1});
        let data = json!({"query": s, "variables": param});
        self.0.post::<ResultGraphql<UserListPage>, Value>("/graphql", data, done)
    }
}