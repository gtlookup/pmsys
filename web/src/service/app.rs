use crate::service::request::Http;
use crate::AppConfig;
use crate::error::Error;
use yew::Callback;
use yew::services::fetch::FetchTask;

type _Result = Callback<Result<AppConfig, Error>>;

pub struct AppService(Http);

impl AppService {
    pub fn new() -> Self { Self(Http::new()) }

    pub fn load_api(&mut self, done: _Result) -> FetchTask {
        self.0.get::<AppConfig>("/app.json", done)
    }
}