use yew_router::Switch;

#[derive(Switch, Clone, PartialEq, Debug)]
// pub enum WebRouter {
//     #[to = "/#/home"]
//     Home,
//     #[to = "/#/user"]
//     User,
//     #[to = "/#/"]
//     Login
// }
pub enum WebRouter {
    #[to = "/home"]
    Home,
    #[to = "/user"]
    User,
    #[to = "/"]
    Login
}