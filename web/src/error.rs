use thiserror::Error as ThisError;
use serde_json::Value;
use serde::Deserialize;

#[derive(ThisError, Clone, Debug)]
pub enum Error {
    /// 401
    #[error("Unauthorized")]
    Unauthorized,

    /// 403
    #[error("Forbidden")]
    Forbidden,

    /// 404
    #[error("Not Found")]
    NotFound,

    /// 422
    #[error("Unprocessable Entity")]
    UnprocessableEntity,

    /// 500
    #[error("Internal Server Error")]
    InternalServerError,

    /// serde deserialize error
    #[error("Deserialize Error")]
    DeserializeError,

    /// request error
    #[error("Http Request Error")]
    RequestError,

    #[error("")]
    GraphqlError(GraphqlError)
}

#[derive(Debug, Deserialize, Clone)]
pub struct GraphqlErrorItem {
    pub extensions: Option<Value>,
    pub message: String,
    pub locations: Option<Vec<Value>>,
    pub path: Vec<String>
}

/// 调 async-graphql 返回的错误信息
#[derive(Debug, Deserialize, Clone)]
pub struct GraphqlError { pub errors: Vec<GraphqlErrorItem> }