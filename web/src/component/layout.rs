
use yew::prelude::*;
use yew_router::components::RouterAnchor;
use crate::router::WebRouter;
use yew_router::prelude::RouteService;

pub enum LayoutAction {
    Logout
}

pub struct Layout(ComponentLink<Self>);

impl Component for Layout {
    type Message = LayoutAction;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        crate::layout_init();
        Self(link)
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            LayoutAction::Logout => {
                crate::utils::state::remove_token();
                crate::utils::redirect::to(WebRouter::Login)
            }
        }
        false
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        html! {
            <>
                { self.header() }
                { self.menu() }
            </>
        }
    }
}

impl Layout {
    fn header(&self) -> Html {
        html! {
            <header class="app-header">
                <a class="app-header__logo" href="#">{"PMsys"}</a>
                <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
                <ul class="app-nav">
                    // <li class="app-search">
                    //     <input class="app-search__input" type="search" placeholder="Search" />
                    //     <button class="app-search__button"><i class="fa fa-search"></i></button>
                    // </li>
                    // <li class="dropdown"><a class="app-nav__item" href="javascript:void(0)"><i class="fa fa-bell-o fa-lg"></i></a></li>
                    {self.profile()}
                </ul>
            </header>
        }
    }
    fn profile(&self) -> Html {
        let logout = self.0.callback(|_| LayoutAction::Logout);
        html! {
            <li class="dropdown">
                <a class="app-nav__item" href="javascript:void(0)" data-toggle="dropdown"><i class="fa fa-user fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu dropdown-menu-right">
                    // <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> {"Settings"}</a></li>
                    // <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> {"Profile"}</a></li>
                    <li>
                        <a class="dropdown-item" href="javascript:void(0)" onclick=logout>
                            <i class="fa fa-sign-out fa-lg"></i> {"注销"}
                        </a>
                    </li>
                </ul>
            </li>
        }
    }
    fn menu(&self) -> Html {
        type A = RouterAnchor<WebRouter>;
        html! {
            <aside class="app-sidebar">
                <ul class="app-menu">
                    <li>
                        <A classes={self.menu_active(WebRouter::Home) + " app-menu__item"} route=WebRouter::Home>
                            <i class="app-menu__icon fa fa-laptop"></i>
                            <span class="app-menu__label">{"看板"}</span>
                        </A>
                    </li>
                    <li>
                        <A classes={self.menu_active(WebRouter::User) + " app-menu__item"} route=WebRouter::User>
                            <i class="app-menu__icon fa fa-user"></i>
                            <span class="app-menu__label">{"人员"}</span>
                        </A>
                    </li>
                </ul>
            </aside>
        }
    }

    fn menu_active(&self, route: WebRouter) -> String {
        let p = RouteService::<WebRouter>::new().get_path();
        let s = "active".to_string();

        match route {
            WebRouter::Home if p.eq("/home") => s,
            WebRouter::User if p.eq("/user") => s,
            _ => String::new()
        }
    }
}