use yew::prelude::*;

#[derive(Properties, Clone, PartialEq, Debug)]
pub struct Props {
    pub id: &'static str,
    pub index: usize,
    pub then: Callback<usize>
}

pub struct Confirm {
    props: Props
}

impl Component for Confirm {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let cb = self.props.then.clone();
        let index = self.props.index;
        let yes = Callback::from(move |_| cb.emit(index));
        html! {
            <div class="modal fade" id={self.props.id} data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title"><i class="fa fa-question-circle-o text-warning"></i></h1>
                        </div>
                        <div class="modal-body"><h5>{"是否继续些操作？"}</h5></div>
                        <div class="modal-footer">
                            <button onclick=yes class="btn btn-primary" type="button">{"是"}</button>
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">{"否"}</button>
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}