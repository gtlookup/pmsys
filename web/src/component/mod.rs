use yew::Html;
use yew::prelude::*;

pub mod layout;
pub mod pagination;
pub mod confirm;

pub fn row(s: Html) -> Html {
    html! {
        <div class="row">
            <div class="col-12">{s}</div>
        </div>
    }
}

pub fn card(s: Html) -> Html {
    html! {
        <div class="row">
            <div class="col-12">
                <div class="tile">
                    <div class="tile-body">{ s }</div>
                </div>
            </div>
        </div>
    }
}