
use yew::prelude::*;
use crate::component::layout::Layout;


pub struct HomeView;

impl Component for HomeView {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, _: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        html! {
            <>
                <Layout />
                <main class="app-content">
                    <div class="app-title">
                        <div>
                            <h1><i class="fa fa-dashboard"></i>{" 看板"}</h1>
                            <p>{"A free and open source Bootstrap 4 admin template"}</p>
                        </div>
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                            <li class="breadcrumb-item"><a href="#">{"Home"}</a></li>
                        </ul>
                    </div>
                </main>
            </>
        }
    }
}