#![recursion_limit="1024"]

use wasm_bindgen::prelude::*;
use std::fmt::Debug;
use maplit::hashmap;
use lazy_static::lazy_static;
use std::collections::HashMap;

pub mod view;
pub mod router;
pub mod app;
pub mod component;
pub mod utils;
pub mod service;
pub mod error;
pub mod ob;

#[wasm_bindgen]
extern {
    /// 弹错误消息
    pub fn success(s: &str);
    /// 弹错误消息
    pub fn error(s: &str);
    /// 初始化 layout 动态效果
    pub fn layout_init();
    /// 确认框
    pub fn confirm(s: String);
    /// 关闭 modal
    pub fn close_modal(s: &str);
    /// 弹 swal 消息框
    /// b: true -> success, false -> false
    /// t: title
    /// s: message
    pub fn swl_message(b: bool, t: &str, s: &str);
    /// 清空select选中项目（因为yew有时不好用，所以在js里控制）
    pub fn select_clean(s: &str);
}

lazy_static! {
    pub static ref USER_POS: HashMap<u32, &'static str> = hashmap! {
        1 => "admin",
        2 => "boss",
        3 => "manager",
        4 => "leader",
        5 => "worker"
    };
}

/// app.json
#[derive(serde::Deserialize, Debug)]
pub struct AppConfig { pub api: String }
/// 打印
pub fn log<T>(s: T) where T: Debug {
    yew::services::ConsoleService::log(format!("{:?}", s).as_str())
}

#[wasm_bindgen(start)]
pub fn run() {
    yew::start_app::<app::App>();
}