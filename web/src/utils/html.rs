
pub fn err_class_msg(s: String) -> (String, String) {
    if s.is_empty() {
        (
            htmlescape::decode_html("&nbsp;").unwrap(),
            "form-control".to_string()
        )
    } else { (s, "form-control is-invalid".to_string()) }
}