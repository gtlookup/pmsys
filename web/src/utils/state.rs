use yew::services::StorageService;
use yew::services::storage::Area;

const TOKEN: &str = "__token";
const API: &str = "__api";

fn area(e: Area) -> StorageService {
    StorageService::new(e).ok().unwrap()
}
pub fn set_local(k: &str, s: String) {
    let mut local = area(Area::Local);
    local.store(k, Ok(s));
}
pub fn get_local(k: &str) -> Option<String> {
    match area(Area::Local).restore(k) {
        Ok(v) => Some(v),
        _ => None
    }
}
pub fn remove_local(k: &str) {
    let mut local = area(Area::Local);
    local.remove(k);
}

pub fn get_token() -> Option<String> { get_local(TOKEN) }
pub fn set_token(s: String) { set_local(TOKEN, s) }
pub fn remove_token() { remove_local(TOKEN) }

pub fn get_api() -> Option<String> { get_local(API) }
pub fn set_api(s: String) { set_local(API, s) }