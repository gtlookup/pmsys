use chrono::{DateTime, Local};
use std::time::{Duration, UNIX_EPOCH};

/// timestamp 转成本地日期字符串
pub fn from_timestamp(dt: Option<u32>) -> String {
    match dt {
        Some(v) => {
            let s = "%Y/%m/%d %H:%M:%S";
            let d = UNIX_EPOCH + Duration::from_secs(v as u64);
            DateTime::<Local>::from(d).format(s).to_string()
        },
        None => String::new()
    }
}

/// 年月日时分秒
pub fn ymd_hms() -> String {
    Local::now().format("%Y%m%d%H%M%S").to_string()
}