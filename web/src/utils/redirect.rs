use yew_router::RouterState;
use yew_router::agent::{RouteAgentDispatcher, RouteRequest};
use crate::router::WebRouter;

#[derive(Debug)]
struct Redirect<STATE: RouterState = ()> { router: RouteAgentDispatcher<STATE> }
impl Redirect {
    fn to(to: WebRouter) {
        let mut me = Self { router: RouteAgentDispatcher::new() };
        let r = yew_router::route::Route::from(to);
        me.router.send(RouteRequest::ChangeRoute(r));
    }
}

pub fn to(to: WebRouter) { Redirect::to(to) }